<?php

namespace shipmonktask\list\node;

use PHPUnit\Framework\TestCase;
use shipmonktask\list\sorter\IntSorter;
use shipmonktask\list\sorter\StringSorter;

/**
 * @covers \shipmonktask\list\node\SingleNode
 * @covers \shipmonktask\list\node\SortedLinkedList
 */
class SortedLinkedListTest extends TestCase
{
	public static function addIntDataprovider()
	{
		return [
			'default' => [
				[10, -1098, 16, 123, -123, 123], '-1098|-123|10|16|123|123'
			],
		];
	}

	public static function addStringDataprovider()
	{
		return [
			'default' => [
				['1', '-1098', 'jkj', 'iudjsadjn', 'uenut934', 'uenut924', 'uenut-934', 'uenut234', 'uenut934',], '-1098|1|iudjsadjn|jkj|uenut-934|uenut234|uenut924|uenut934|uenut934'
			],
			'czech (ignored locale for test)' => [
				['Nechť', 'již', 'hříšné', 'saxofony', 'ďáblů', 'rozezvučí', 'síň', 'úděsnými', 'tóny', 'waltzu', ',', 'tanga', 'a', 'quickstepu'], ',|a|hříšné|již|Nechť|quickstepu|rozezvučí|saxofony|síň|tanga|tóny|waltzu|úděsnými|ďáblů'
			],
		];
	}

	/**
	 * @dataProvider addIntDataprovider
	 * @param int[] $nodes
	 * @covers \shipmonktask\list\node\SortedLinkedList::add()
	 * @covers \shipmonktask\list\sorter\IntSorter
	 */
	public function testAddInt(array $nodes, string $expected): void
	{
		$sortedLinkedList = new SortedLinkedList(
			new IntSorter
		);

		foreach ($nodes as $node) {
			$sortedLinkedList->add(new SingleNode($node));
		}

		$result = '';
		$node = $sortedLinkedList->head;
		while ($node) {
			$result .= $node->value;

			if ($node = $node->next) {
				$result .= '|';
			}
		}
		self::assertEquals($expected, $result);
	}

	/**
	 * @dataProvider addStringDataprovider
	 * @param string[] $nodeValues
	 * @covers \shipmonktask\list\node\SortedLinkedList::add()
	 * @covers \shipmonktask\list\sorter\StringSorter
	 */
	public function testAddString(array $nodeValues, string $expected): void
	{
		setlocale(LC_COLLATE, 'cs_CZ.ISO8859-2');
		$sortedLinkedList = new SortedLinkedList(
			new StringSorter
		);

		foreach ($nodeValues as $node) {
			$sortedLinkedList->add(new SingleNode($node));
		}

		$result = '';
		$node = $sortedLinkedList->head;
		while ($node) {
			$result .= (string)$node->value;

			if ($node = $node->next) {
				$result .= '|';
			}
		}
		self::assertEquals($expected, $result);
	}

}
