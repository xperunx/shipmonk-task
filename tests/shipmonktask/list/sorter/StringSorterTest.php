<?php declare(strict_types=1);

namespace shipmonktask\list\sorter;

use PHPUnit\Framework\TestCase;
use shipmonktask\list\node\SingleNode;

/**
 * @covers \shipmonktask\list\sorter\StringSorter
 */
class StringSorterTest extends TestCase
{
	private StringSorter $sorter;

	public function __construct(string $name)
	{
		parent::__construct($name);
		$this->sorter = new StringSorter();
	}

	public static function sortDataProvider(): array
	{
		return [
			'a < b' => [new SingleNode('a'), new SingleNode('b'), -1],
			'b > a' => [new SingleNode('b'), new SingleNode('a'), 1],
			'a === a' => [new SingleNode('a'), new SingleNode('a'), 0],
			'žluťoučký kůň > úpěl ďábelské ódy' => [new SingleNode('žluťoučký kůň'), new SingleNode('úpěl ďábelské ódy'), 1],
			'úpěl ďábelské ódy === úpěl ďábelské ódy' => [new SingleNode('úpěl ďábelské ódy'), new SingleNode('úpěl ďábelské ódy'), 0],
			'žluťoučký kůň === žluťoučký kůň' => [new SingleNode('žluťoučký kůň'), new SingleNode('žluťoučký kůň'), 0],
		];
	}

	/**
	 * @dataProvider sortDataProvider
	 * @covers \shipmonktask\list\sorter\StringSorter::sort()
	 */
	public function testSort(SingleNode $value1, SingleNode $value2, int $expectedValue): void
	{
		$this->assertEquals($expectedValue, $this->sorter->sort($value1, $value2));
	}
}
