<?php declare(strict_types=1);

namespace shipmonktask\list\sorter;

use PHPUnit\Framework\TestCase;
use shipmonktask\list\node\SingleNode;

class IntSorterTest extends TestCase
{

	private IntSorter $sorter;

	public function __construct(string $name)
	{
		$this->sorter = new IntSorter;
		parent::__construct($name);
	}

	public static function sortDataProvider()
	{
		return [
			'1 > 0' => [new SingleNode(1), new SingleNode(0), 1],
			'-1 < 0' => [new SingleNode(-1), new SingleNode(0), -1],
			'0 <>> 1' => [new SingleNode(0), new SingleNode(1), -1],
			'0 > -1' => [new SingleNode(1), new SingleNode(0), 1],
			'0 === 0' => [new SingleNode(0), new SingleNode(0), 0],
			'-100 === -100' => [new SingleNode(-100), new SingleNode(-100), 0],
			'100 === 100' => [new SingleNode(100), new SingleNode(100), 0],
			'PHP_INT_MAX === PHP_INT_MAX' => [new SingleNode(PHP_INT_MAX), new SingleNode(PHP_INT_MAX), 0],
		];
	}

	/**
	 * @dataProvider sortDataProvider
	 * @covers \shipmonktask\list\sorter\IntSorter::sort()
	 */
	public function testSort(SingleNode $intNode1, SingleNode $intNode2, $expectedValue): void
	{
		$this->assertEquals($expectedValue, $this->sorter->sort($intNode1, $intNode2));
	}
}
