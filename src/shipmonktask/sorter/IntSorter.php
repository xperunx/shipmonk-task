<?php declare(strict_types=1);

namespace shipmonktask\list\sorter;

use shipmonktask\list\node\SingleNode;

class IntSorter implements Sorter
{

	public function sort(SingleNode $a, SingleNode $b): int
	{
		return ($a < $b) ? -1 : (($a > $b) ? 1 : 0);
	}
}
