<?php declare(strict_types=1);

namespace shipmonktask\list\sorter;

use shipmonktask\list\node\SingleNode;

class StringSorter implements Sorter
{
    public function sort(SingleNode $value1, SingleNode $value2): int
    {

        return strnatcasecmp(
            (string)$value1->value,
            (string)$value2->value
        );
    }
}
