<?php

namespace shipmonktask\list\sorter;

use shipmonktask\list\node\SingleNode;

interface Sorter
{
	/**
	 * @param SingleNode $value1
	 * @param SingleNode $value2
	 * @return int Comparison value 1|0|-1 for value1 is less|equal|great than value2
	 */
	public function sort(SingleNode $value1, SingleNode $value2): int;
}
