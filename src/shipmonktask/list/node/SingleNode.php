<?php declare(strict_types=1);

namespace shipmonktask\list\node;

class SingleNode
{
	public function __construct(
		public string|int  $value,
		public ?SingleNode $next = null
	)
	{

	}
}
