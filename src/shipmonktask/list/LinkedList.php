<?php declare(strict_types=1);

namespace shipmonktask\list\node;

interface LinkedList
{
	public function add(SingleNode $node): self;

}
