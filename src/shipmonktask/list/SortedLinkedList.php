<?php

namespace shipmonktask\list\node;

use shipmonktask\list\sorter\Sorter;

class SortedLinkedList implements LinkedList
{
	public function __construct(
		private readonly Sorter $sorter,
		public ?SingleNode      $head = null
	)
	{

	}

	public function add(SingleNode $node): self
	{
		if ($this->head === null || $this->sorter->sort($this->head, $node) > -1) {
			$node->next = $this->head;
			$this->head = $node;
		} else {
			$currentNode = $this->head;
			while ($currentNode->next !== null && $this->sorter->sort($currentNode->next, $node) === -1) {
				$currentNode = $currentNode->next;
			}
			$node->next = $currentNode->next;
			$currentNode->next = $node;
		}

		return $this;
	}
}
