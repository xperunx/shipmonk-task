README
------

Issue
=====
Implement from scratch a fully working project with SortedLinkedList (linked list that keeps values sorted). Implement only string and int values for example purposes.

Solution
========
- implemented Single Linked list
- implemented sorter for int and string with one direction
- usage via phpunit test
- added only PHPStan with default settings

TODOs (time limit :( )
=====
- add remove node 
- add get node by value
- write tests for new methods 
- eventually implement double linked list with SPL PHP library
- write usage docs 

Possible TODOs
==============
- add phpcs

Usage
=====
```shell
composer install
composer phpunit
```

